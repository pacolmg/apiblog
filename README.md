API BLOG
========================

Example of an API made for a blog.


Install Project
--------------

* Clone it
* Composer install. Add the data for the database when the process is done.
* Launch the command to create the database: php bin/console doctrine:database:create
* Launch the command to create the database schema: php bin/console doctrine:schema:create
* Load the data for the example: php bin/console doctrine:fixtures:load
* Afther doing this there will be 100 posts in database. You can access them via browser in the url /app_dev.php/noticia
* The documentation will be in /api/doc