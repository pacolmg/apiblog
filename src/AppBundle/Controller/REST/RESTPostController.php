<?php

namespace AppBundle\Controller\REST;

use AppBundle\Entity\Post;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Post controller.
 *
 */
class RESTPostController extends FOSRestController
{
    /**
     *
     * @Rest\Get("/api/noticia/list")
     * @SWG\Response(
     *     response=200,
     *     description="Devuelve un listado de posts",
     *     @SWG\Schema(
     *         type="array"
     *     )
     * )
     * @SWG\Tag(name="Noticias")
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('AppBundle:Post')->findAll();

        return $this->view($posts, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/api/noticia/new")
     * @SWG\Parameter(
     *     name="title",
     *     in="header",
     *     type="string",
     *     description="Título de la Noticia"
     * )
     * @SWG\Parameter(
     *     name="body",
     *     in="header",
     *     type="string",
     *     description="Cuerpo de la Noticia"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Inserta  un nuevo post",
     *     @SWG\Schema(
     *         type="array"
     *     )
     * )
     * @SWG\Tag(name="Noticias")
     */
    public function newAction(Request $request)
    {
        $data = new Post();
        $title = $request->get('title');
        $body = $request->get('body');

        $data->setTitle($title);
        $data->setBody($body);
        $data->setCreatedAt(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return $this->view("Post Added Successfully", Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/api/noticia/{id}/show")
     * @SWG\Parameter(
     *     name="id",
     *     in="query",
     *     type="integer",
     *     description="ID de la Noticia"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Devuelve un post",
     *     @SWG\Schema(
     *         type="array"
     *     )
     * )
     * @SWG\Tag(name="Noticias")
     */
    public function showAction($id)
    {
        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);
        if (empty($post)) return $this->view('No encontrado', Response::HTTP_NOT_FOUND);

        return $this->view($post, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/api/noticia/{id}/edit")
     * @SWG\Parameter(
     *     name="id",
     *     in="query",
     *     type="integer",
     *     description="ID de la Noticia"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Edita un post",
     *     @SWG\Schema(
     *         type="array"
     *     )
     * )
     * @SWG\Tag(name="Noticias")
     */
    public function editAction(Request $request, $id)
    {
        $title = $request->get('title');
        $body = $request->get('body');

        $sn = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);
        if (empty($post)) {
            return $this->view("Post not found", Response::HTTP_NOT_FOUND);
        }
        elseif(!empty($title) && !empty($body)){
            $post->setTitle($title);
            $post->setBody($body);
            $sn->flush();
            return $this->view("Post Updated Successfully", Response::HTTP_OK);
        }
        else return $this->view("Título y cuerpo cannot be empty", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @Rest\Delete("/api/noticia/{id}/delete")
     * @SWG\Parameter(
     *     name="id",
     *     in="query",
     *     type="integer",
     *     description="ID de la Noticia"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Elimina un post",
     *     @SWG\Schema(
     *         type="array"
     *     )
     * )
     * @SWG\Tag(name="Noticias")
     */
    public function deleteAction(Request $request, $id)
    {
        $sn = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);
        if (empty($post)) {
            return new View("post not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($post);
            $sn->flush();
        }
        return $this->view("deleted successfully", Response::HTTP_OK);
    }

}
