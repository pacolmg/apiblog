<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Post;

class LoadPostData implements FixtureInterface
{

    /**
    *
    * @param ObjectManager $manager
    */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 100; $i++) {
            $this->createBlogPost($manager, $i);
        }
    }

    /**
    *
    * @param ObjectManager $manager
    * @param type $n
    */
    private function createBlogPost(ObjectManager $manager, $n)
    {
        $post = new Post();
        $post->setTitle('Post ' . $n);
        $post->setBody('<p>Lorem Ipsum es simplemente el texto de relleno de las '
            . 'imprentas y archivos de texto. Lorem Ipsum ha sido el texto de '
            . 'relleno estándar de las industrias desde el año 1500, cuando un impresor</p>'
        . '<p>Lorem Ipsum es simplemente el texto de relleno de las '
            . 'imprentas y archivos de texto. Lorem Ipsum ha sido el texto de '
            . 'Ipsum es simplemente el texto de relleno de las '
            . 'imprentas y archivos de texto. Lorem Ipsum ha sido el texto de '
            . 'relleno estándar de las industrias desde el año 1500, cuando un impresor</p>');


        $int = mt_rand(1262055681, 1562055681);

        $string = date("Y-m-d H:i:s", $int);
        $date = new \DateTime($string);

        $post->setCreatedAt($date);

        $manager->persist($post);
        $manager->flush();
    }


}